﻿require.config({
    paths: {
        "jquery": "bower_components/jquery/dist/jquery",
        "common": "js/common",
        "css":"bower_components/require-css/css",
        "bootstrap-js": "bower_components/bootstrap/dist/js/bootstrap"

    },
    config: {
        "common":{
            homePartial:[{
                'sectionId':'header',
                'partialUrl':'partials/_header.html'
            },
            {
                'sectionId': 'footer',
                'partialUrl': 'partials/_footer.html'
            }]
        }
    },
    shim: {
        'bootstrap-js': {
            deps:['jquery']
        },
        'jquery': {
            exports: 'jquery',
        }
    }
})
require(['common'])